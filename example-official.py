import time
import mujoco
from mujoco import viewer

xml = """
<mujoco>
    <option gravity="0 0 -9.8"/>
    <worldbody>
        <light name="top" pos="0 0 1"/>
        <body name="box_and_sphere" euler="0 0 -30">
            <joint name="swing" type="hinge" axis="1 -1 0" pos="-.2 -.2 -.2"/>
            <geom name="red_box" type="box" size=".2 .2 .2" rgba="1 0 0 1"/>
            <geom name="green_sphere" pos=".2 .2 .2" size=".1" rgba="0 1 0 1"/>
        </body>
    </worldbody>
</mujoco>
"""

tippe_top = """
<mujoco model="tippe top">
  <option integrator="RK4"/>

  <asset>
    <texture name="grid" type="2d" builtin="checker" rgb1=".1 .2 .3"
     rgb2=".2 .3 .4" width="300" height="300"/>
    <material name="grid" texture="grid" texrepeat="8 8" reflectance=".2"/>
  </asset>

  <worldbody>
    <geom size=".2 .2 .01" type="plane" material="grid"/>
    <light pos="0 0 .6"/>
    <camera name="closeup" pos="0 -.1 .07" xyaxes="1 0 0 0 1 2"/>
    <body name="top" pos="0 0 .02">
      <freejoint/>
      <geom name="ball" type="sphere" size=".02" />
      <geom name="stem" type="cylinder" pos="0 0 .02" size="0.004 .008"/>
      <geom name="ballast" type="box" size=".023 .023 0.005"  pos="0 0 -.015"
       contype="0" conaffinity="0" group="3"/>
    </body>
  </worldbody>

  <keyframe>
    <key name="spinning" qpos="0 0 0.02 1 0 0 0" qvel="0 0 0 0 1 200" />
  </keyframe>
</mujoco>
"""

# 空格暂停
pause = False


def key_callback(keycode):
    if chr(keycode) == " ":
        global pause
        pause = not pause


model = mujoco.MjModel.from_xml_string(tippe_top)
data = mujoco.MjData(model)
# 暂时没显示出来joint
scene_option = mujoco.MjvOption()
scene_option.flags[mujoco.mjtVisFlag.mjVIS_JOINT] = True

mujoco.mj_resetDataKeyframe(model, data, 0)
# 被动方式启动查看器，不阻塞用户代码，返回交互句柄，用于和查看器交互
with viewer.launch_passive(model, data, key_callback=key_callback) as viewer_handle:
    while viewer_handle.is_running():
        step_start = time.time()
        if not pause:
            mujoco.mj_step(model, data)
            viewer_handle.sync()
            # 休眠10ms，系统时钟分辨率大于2ms，time.time()检测不出来
            time.sleep(0.01)